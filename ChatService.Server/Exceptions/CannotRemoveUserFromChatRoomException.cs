﻿using System;
using System.Runtime.Serialization;

namespace ChatService.Server.Exceptions
{
    [Serializable]
    internal class CannotRemoveUserFromChatRoomException : Exception
    {
        public CannotRemoveUserFromChatRoomException()
        {
        }

        public CannotRemoveUserFromChatRoomException(string message) : base(message)
        {
        }

        public CannotRemoveUserFromChatRoomException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CannotRemoveUserFromChatRoomException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}