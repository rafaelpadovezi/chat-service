﻿using ChatService.Server.Commands;

namespace ChatService.Server.Messages
{

    public class ChatServerResponse
    {
        public string Message { get; set; }
        public ResponseType Type { get; set; }
        public string[] Details { get; set; }

        public static ChatServerResponse Help() =>
            new ChatServerResponse
            {
                Type = ResponseType.Help,
                Details = CommandParser.CommandList
            };

        public static ChatServerResponse UsernameIsTaken(string username) =>
            new ChatServerResponse
            {
                Type = ResponseType.UsernameIsTaken,
                Message = $"*** Sorry, the nickname {username} is already" +
                    $"taken! please choose a different one:"
            };

        public static ChatServerResponse Registered(string nickname, string chatName) =>
            new ChatServerResponse
            {
                Type = ResponseType.Registrated,
                Message = $"*** You are registered as {nickname}. Joining #{chatName}."
            };

        public static ChatServerResponse Welcome() =>
            new ChatServerResponse
            {
                Type = ResponseType.Welcome,
                Message = $"*** Welcome to out chat server. Please provide a nickname:"
            };

        internal static ChatServerResponse ListChatRooms(string[] chatRoomList) =>
            new ChatServerResponse
            {
                Type = ResponseType.ListChatRooms,
                Details = chatRoomList
            };
    }
}
