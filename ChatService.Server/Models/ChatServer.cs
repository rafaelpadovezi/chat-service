﻿using ChatService.Server.Commands;
using ChatService.Server.Messages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;

namespace ChatService.Server.Models
{
    public class ChatServer
    {
        private readonly ChatRoom _general = new ChatRoom("general");
        private readonly ConcurrentDictionary<string, ChatUser> _users =
            new ConcurrentDictionary<string, ChatUser>();
        private readonly ConcurrentDictionary<string, ChatRoom> _chatRooms =
            new ConcurrentDictionary<string, ChatRoom>();
        private readonly ILogger<ChatServer> _logger;

        public ChatServer(ILogger<ChatServer> logger)
        {
            _logger = logger;
            _chatRooms.TryAdd(_general.Name, _general);
        }

        public string[] ChatRoomList =>
            _chatRooms.Values.Select(x => x.Name).ToArray();

        internal async Task NewConnection(WebSocket webSocket)
        {
            using var scope = _logger.BeginScope(Guid.NewGuid());
            ChatUser user = await ConnectUserAsync(webSocket);
            ChatRoom currentChatRoom = _general;

            while (true)
            {
                var (run, newChatRoom) = await AddUserToChatAsync(currentChatRoom, user);
                await currentChatRoom.RemoveUserAsync(user);
                if (!run)
                    break;
                if (newChatRoom != null)
                    currentChatRoom = newChatRoom;
            };

            await DisconnectUserAsync(webSocket, user);
        }

        private async Task<(bool, ChatRoom)> AddUserToChatAsync(
            ChatRoom  chatRoom, ChatUser user)
        {
            
            await chatRoom.AddUserAsync(user);

            while (user.IsConnected)
            {
                var command = await user.ReceiveCommandAsync();
                if (command.Type == CommandType.Help)
                    await user.SendAsync(ChatServerResponse.Help());
                else if (command.Type == CommandType.ListChatRooms)
                    await user.SendAsync(ChatServerResponse.ListChatRooms(ChatRoomList));
                else if (command.Type == CommandType.ChangeChatRoom)
                {
                    if (command.ChatRoomName == chatRoom.Name)
                        continue;
                    var newChatRoom = _chatRooms.GetOrAdd(command.ChatRoomName,
                        new ChatRoom(command.ChatRoomName));
                    return (true, newChatRoom);
                }
                else if (command.Type == CommandType.Exit || !user.IsConnected)
                    return (false, null);
                else
                    await chatRoom.SendMessageAsync(user, command);
            }
            return (true, null);
        }

        private async Task<ChatUser> ConnectUserAsync(WebSocket webSocket)
        {
            _logger.LogInformation("User connected");
            var user = new ChatUser(webSocket);
            await user.SendAsync(ChatServerResponse.Welcome());

            string nickname = await GetUserNicknameAsync(user);

            user.Nickname = nickname;
            await user.SendAsync(
                ChatServerResponse.Registered(nickname, _general.Name));
            return user;
        }

        private async Task DisconnectUserAsync(WebSocket webSocket, ChatUser user)
        {
            while (!_users.TryRemove(user.Nickname, out _))
            {
                await Task.Delay(100);
                if (!_users.Values.Any(x => x.Nickname == user.Nickname))
                    break;
            };
            await webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure,
                "Done", CancellationToken.None);
            _logger.LogInformation("User disconnected");
        }

        private async Task<string> GetUserNicknameAsync(ChatUser user)
        {
            while (true)
            {   
                string nickname = await user.ReceiveStringAsync();
                if (_users.TryAdd(nickname, user)) 
                    return nickname;
                await user.SendAsync(
                    ChatServerResponse.UsernameIsTaken(nickname));
            }
        }
    }
}
