﻿using ChatService.Server.Commands;
using ChatService.Server.Extensions;
using ChatService.Server.Messages;
using System.Net.WebSockets;
using System.Threading.Tasks;

namespace ChatService.Server.Models
{
    internal class ChatUser
    {
        public ChatUser(WebSocket webSocket)
        {
            WebSocket = webSocket;
        }

        public string Nickname { get; set; }
        private WebSocket WebSocket { get; }
        public bool IsConnected =>
            WebSocket.State != WebSocketState.CloseReceived &&
            WebSocket.State != WebSocketState.Closed &&
            WebSocket.State != WebSocketState.Aborted;

        public async Task SendAsync(ChatServerResponse serverResponse) =>
            await WebSocket.SendJsonAsync(serverResponse);

        public async Task<ChatCommand> ReceiveCommandAsync() =>
            CommandParser.Parse(
                await WebSocket.ReceiveStringAsync());

        public async Task<string> ReceiveStringAsync() =>
            await WebSocket.ReceiveStringAsync();
    }
}