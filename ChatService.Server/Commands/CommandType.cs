﻿namespace ChatService.Server.Commands
{
    public enum CommandType
    {
        PublicMessage,
        PublicMessageToUser,
        PrivateMessageToUser,
        ListChatRooms,
        ChangeChatRoom,
        Kudos,
        Exit,
        Error,
        Help
    }
}