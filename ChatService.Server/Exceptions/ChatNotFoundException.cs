﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatService.Server.Exceptions
{
    public class ChatNotFoundException : Exception
    {
        public ChatNotFoundException(string chatname)
            : base($"Chat {chatname} not found!")
        {
        }
    }
}
