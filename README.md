# Chat Service

Servi�o de bate papo com cliente Web.

## Comandos

- `\u {user}` - Envia mensagem p�blica para um usu�rio
- `\p {user}` - Envia mensagem privada para um usu�rio
- `\kudos {user}` - Envia *Kudos* para um usu�rio
- `\list` - Mostra a lista de salas de bate papo
- `\c {chatroom}` - Troca de sala de bate papo
- `\help` - Mostra a lista de comandos