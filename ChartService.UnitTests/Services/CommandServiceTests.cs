﻿using ChatService.Server.Commands;
using NUnit.Framework;

namespace ChartService.UnitTests.Services
{
    public class CommandServiceTests
    {
        [Test]
        public void Parse_PublicMessage()
        {
            var command = CommandParser.Parse("Oi!");

            Assert.AreEqual(CommandType.PublicMessage, command.Type);
            Assert.AreEqual("Oi!", command.Message);
        }

        [Test]
        public void Parse_PublicMessageToUser()
        {
            var command = CommandParser.Parse(@"\u user1 Hi user1!");

            Assert.AreEqual(CommandType.PublicMessageToUser, command.Type);
            Assert.AreEqual("user1", command.Destinatary);
            Assert.AreEqual("Hi user1!", command.Message);
        }

        [Test]
        public void Parse_PrivateMessageToUser()
        {
            var command = CommandParser.Parse(@"\p user1 Hi user1!");

            Assert.AreEqual(CommandType.PrivateMessageToUser, command.Type);
            Assert.AreEqual("user1", command.Destinatary);
            Assert.AreEqual("Hi user1!", command.Message);
        }

        [Test]
        public void Parse_Exit()
        {
            var command = CommandParser.Parse(@"\exit");

            Assert.AreEqual(CommandType.Exit, command.Type);
        }

        [TestCase(@"\invalid", @"InvalidCommand \invalid")]
        [TestCase(@"\invalid 123", @"InvalidCommand \invalid")]
        [TestCase(@"\", @"InvalidCommand \")]
        public void Parse_InvalidCommand(string commandString, string messageResult)
        {
            var command = CommandParser.Parse(commandString);

            Assert.AreEqual(CommandType.Error, command.Type);
            Assert.AreEqual(messageResult, command.Message);
        }

        [Test]
        public void Parse_Kudos()
        {
            var command = CommandParser.Parse(@"\kudos user1");

            Assert.AreEqual(CommandType.Kudos, command.Type);
            Assert.AreEqual("user1", command.Destinatary);
        }

        [Test]
        public void Parse_Kudos_WithoutDestinatary()
        {
            var command = CommandParser.Parse(@"\kudos");

            Assert.AreEqual(CommandType.Error, command.Type);
            Assert.AreEqual("Command must have destinatary", command.Message);
        }

        [Test]
        public void Parse_Help()
        {
            var command = CommandParser.Parse(@"\help");

            Assert.AreEqual(CommandType.Help, command.Type);
            Assert.AreEqual(DestinataryType.Self, command.DestinataryType);
        }

        [Test]
        public void Parse_List()
        {
            var command = CommandParser.Parse(@"\list teste");

            Assert.AreEqual(CommandType.ListChatRooms, command.Type);
            Assert.AreEqual(DestinataryType.Self, command.DestinataryType);
        }

        [Test]
        public void Parse_ChangChatRoom()
        {
            var command = CommandParser.Parse(@"\c chatroom");

            Assert.AreEqual(CommandType.ChangeChatRoom, command.Type);
            Assert.AreEqual(DestinataryType.Self, command.DestinataryType);
            Assert.AreEqual("chatroom", command.ChatRoomName);
        }

        [Test]
        public void Parse_ChangChatRoom_WithoutName()
        {
            var command = CommandParser.Parse(@"\c ");

            Assert.AreEqual(CommandType.Error, command.Type);
            Assert.AreEqual(DestinataryType.Self, command.DestinataryType);
        }
    }
}
