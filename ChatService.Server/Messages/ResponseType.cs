﻿namespace ChatService.Server.Messages
{
    public enum ResponseType
    {
        Welcome,
        Registrated,
        UsernameIsTaken,
        ChatMessage,
        Error,
        Help,
        ListChatRooms
    }
}
