﻿using System;
using System.Runtime.Serialization;

namespace ChatService.Server.Exceptions
{
    [Serializable]
    internal class CannotAddUserToChatRoomException : Exception
    {
        public CannotAddUserToChatRoomException()
        {
        }

        public CannotAddUserToChatRoomException(string message) : base(message)
        {
        }

        public CannotAddUserToChatRoomException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CannotAddUserToChatRoomException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}