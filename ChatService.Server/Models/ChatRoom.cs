﻿using ChatService.Server.Commands;
using ChatService.Server.Exceptions;
using ChatService.Server.Messages;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ChatService.Server.Commands.CommandType;

namespace ChatService.Server.Models
{
    internal class ChatRoom
    {
        private readonly ConcurrentDictionary<string, ChatUser> _users =
            new ConcurrentDictionary<string, ChatUser>();
        private readonly ConcurrentBag<string> _history =
            new ConcurrentBag<string>();

        public ChatRoom(string name) => Name = name;

        public async Task SendMessageAsync(ChatUser user, ChatCommand command)
        {
            var destinataries = GetDestinataries(user, command);
            var message = CreateMessage(user, command);
            await SendMessageAsync(destinataries, message);
        }

        private IEnumerable<ChatUser> GetDestinataries(ChatUser user,
            ChatCommand command) =>
                command.DestinataryType switch
                {
                    DestinataryType.Public => _users.Values,
                    DestinataryType.Private => _users.Values
                        .Where(x => x.Nickname == command.Destinatary
                                 || x == user),
                    _ => new ChatUser[] { user },
                };

        private async Task SendMessageAsync(IEnumerable<ChatUser> destinataries,
            ChatMessage message)
        {
            _history.Add(message.Message);
            await Task.WhenAll(destinataries.Select(x => x.SendAsync(message)));
        }

        internal ChatMessage CreateMessage(ChatUser user, ChatCommand command) =>
            command.Type switch
            {
                Kudos => ChatMessage.Kudos(
                    user.Nickname, command.Destinatary),
                PrivateMessageToUser => ChatMessage.PrivateMessageToUser(
                    user.Nickname, command.Destinatary, command.Message),
                PublicMessageToUser => ChatMessage.PublicMessageToUser(
                    user.Nickname, command.Destinatary, command.Message),
                PublicMessage => ChatMessage.Public(
                    user.Nickname, command.Message),
                _ => ChatMessage.Error(command.Message)
            };

        public string Name { get; }

        public async Task AddUserAsync(ChatUser user)
        {
            if (!_users.TryAdd(user.Nickname, user))
                throw new CannotAddUserToChatRoomException();

            var message = ChatMessage.NewUser(Name, user.Nickname);

            await SendMessageAsync(_users.Values, message);
        }

        public async Task RemoveUserAsync(ChatUser user)
        {
            if (!_users.TryRemove(user.Nickname, out _))
                throw new CannotRemoveUserFromChatRoomException();

            var message = ChatMessage.UserLeftRoom(Name, user.Nickname);

            await SendMessageAsync(_users.Values, message);
        }
    }
}