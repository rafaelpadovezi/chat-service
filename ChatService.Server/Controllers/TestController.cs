using Microsoft.AspNetCore.Mvc;

[Route("test")]
public class TestControler : ControllerBase
{
    [HttpGet]
    public IActionResult Get() => Ok("Ok!");
}