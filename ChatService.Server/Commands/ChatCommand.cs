﻿namespace ChatService.Server.Commands
{
    public class ChatCommand
    {
        public CommandType Type { get; set; }
        public string Message { get; set; }
        public DestinataryType DestinataryType { get; set; }
        public string Destinatary { get; set; }
        public string ChatRoomName { get; set; }
    }
}