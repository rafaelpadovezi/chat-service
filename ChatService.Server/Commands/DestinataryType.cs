﻿namespace ChatService.Server.Commands
{
    public enum DestinataryType
    {
        Self,
        Public,
        Private
    }
}