﻿using System;

namespace ChatService.Server.Messages
{
    public class ChatMessage : ChatServerResponse
    {
        public static ChatMessage Public(string nickname, string message) =>
            new ChatMessage
            {
                Type = ResponseType.ChatMessage,
                Message = $"{nickname} says: {message}"
            };

        public static ChatMessage Error(string message) =>
            new ChatMessage
            {
                Type = ResponseType.Error,
                Message = message
            };

        public static ChatMessage Kudos(string source, string destinatary) =>
            new ChatMessage
            {
                Type = ResponseType.ChatMessage,
                Message = $"{source} sent kudos to {destinatary}: ❤◦.¸¸.  ◦✿"
            };

        public static ChatMessage PublicMessageToUser(string source,
            string destinatary, string message) =>
            new ChatMessage
            {
                Type = ResponseType.ChatMessage,
                Message = $"{source} says to {destinatary}: {message}"
            };

        public static ChatMessage PrivateMessageToUser(string source,
            string destinatary, string message) =>
            new ChatMessage
            {
                Type = ResponseType.ChatMessage,
                Message = $"{source} says privately to {destinatary}: {message}"
            };

        public static ChatMessage NewUser(string chatName, string nickname) =>
            new ChatMessage
            {
                Type = ResponseType.ChatMessage,
                Message = $"\"{nickname}\" has joined #{chatName}"
            };

        public static ChatMessage UserLeftRoom(string chatName, string nickname) =>
            new ChatMessage
            {
                Type = ResponseType.ChatMessage,
                Message = $"\"{nickname}\" has left #{chatName}"
            };
    }
}
